# 21dthd4-2180607483
##Họ tên: Lê Thế Hiển
##MSSV:  2180607483
##Lớp: 21DTHD4
=======

# Họ tên: Lê Thế Hiển

# MSSV:  2180607483

# Lớp: 21DTHD4

| Title | Manager sign in |
| --- | --- |
| Value Statement | As a Manager, I want to sign in to the program  |
| Acceptance Criteria | <ins>Acceptance Criteria 1:</ins><br/>Given that the manager is enter correct password<br/> When the manager presses the login button </br> Then open UI <br/><ins><br/>Acceptance Criteria 2:</ins><br/>Given that the manager is enter incorrect password <br/>When the manager presses the login button</br> Then make sure the login denied message is displayed|
| Definition of Done | Unit Tests Passed <br/> Acceptance Criteria Met <br/> Code Reviewed <br/> Funtional Tests Passed <br/> Non-Funtional Requirements Met <br/> Product Owner Accepts User Story |
| Owner | Le The Hien |
| UI | ![Signin](https://gitlab.com/lethehien.it/21dthd4-2180607483/-/raw/main/Signin.png?ref_type=heads)|

N| Req ID | Test Objected | Test Steps | Expected Result | Actual Result | Pass/Fail | Related Defects |
|-| ---| --- | --- | --- | --- | --- | --- | 
|1| Req-01 | Logged in successfully | 1. Enter correct User. <br/> 2. Enter correct password. <br/> 3. Click the login button. | Log in successfully and go to the working page | | | |
|2| Req-02 | Login failed with wrong password | 1. Enter correct User. <br/> 2. Enter incorrect password <br/> 3. Click the login button. | Login fails and displays the error message “Incorrect password” | | | |
|3| Req-03 | Login failed with wrong User | 1. Enter incorrect User. <br/> 2. Enter correct password. <br/> 3. Click the login button. | Login fails and displays the error message “Incorrect User” | | | |


## Nguyễn Thành Nhân

# Họ tên: Nguyễn Thành Nhân

# MSSV: 2180601029

# Lớp: 21DTHD4

| Title | Manager Add tenant information |
| --- | --- |
| Value Statement | As a manager, I want to add tenant information for vertification as well as management. |
| Acceptance Criteria |<ins>Acceptance Criteria :</ins><br/> Tenant information is not yet available<br/>When the manager enters all information, a new one will be added to the list.|
| Definition of Done | Unit Tests Passed <br/> Acceptance Criteria Met <br/> Code Reviewed <br/> Funtional Tests Passed <br/> Non-Funtional Requirements Met <br/> Product Owner Accepts User Story |
| Owner | Nguyễn Thành Nhân |
| UI | ![ẢnhẢnh](https://gitlab.com/Crimsondd/21dthd4-2180601029/-/raw/main/Retal%20Management%20ADD.png?ref_type=heads)|

|N| Req ID | Test Objected | Test Steps | Expected Result | Actual Result | Pass/Fail | Related Defects |
|-| --- | --- | --- | --- | --- | --- | --- | 
|1| Req-04 | Enter full information | 1. Enter full and correct information. <br/> 2. Click the add button. | All information is added to the list and displays the message “Success!”  | | | |
|2| Req-05 | Enter incorrect information | 1. Enter incorrect information or invalid information. <br/> 2. Click the add button. | Displays the error message "Incorrect information" | | | |
|3| Req-06 | Enter missing information | 1. Enter missing information. <br/> 2. Click the add button. | Displays the error message "Please enter full information" | | | |

| Title | Manager Create lease contract |
| --- | --- |
| Value Statement | As a manager, I want to create rental contracts |
| Acceptance Criteria | <ins>Acceptance Criteria 1:</ins><br/>when customers want to rent a room <br/> Create a contract clearly stating customer information, rights and obligations of the lessee, date of contract formation and contract expiration, signature <br/> <ins>Acceptance Criteria 2:</ins><br/>when the customer wants to renew the contract <br/> Create a contract with the same information as the old contract and add the contract end date |
| Owner | NguyenPhucDat |
| UI | ![TaoHopDong](https://gitlab.com/NguyenPhucDat/21dthd4_2180608489/-/raw/main/datsieudep.jpg?ref_type=heads) |


## Nguyen Hoang Phuc

# Nguyen Hoang Phuc

# 2180608307

# 21DTHD4


| Title | Manager update tenant information |
| --- | --- |
| Value Statement | As a Manager, I want to update tenant information |
| Acceptance Criteria | <ins>Acceptance Criteria 1:</ins><br/>Log in to your manager account on the tenant management platform.<br/>Navigate to the "Tenants" or "Residents" section of the platform.<br/>Search for the specific tenant you want to update information for by entering their name or unit number.<br/>Click on the tenant's profile or select the "Edit" option next to their name.<br/><ins><br/>Acceptance Criteria 2:</ins><br/>Update the necessary information such as contact details, emergency contacts, lease details, or any other relevant data.<br/>Double-check the changes for accuracy and completeness.</br>Save the updated information by clicking on the "Save" or "Update" button.<br/>If applicable, notify the tenant about any changes made to their profile and provide them with the updated information.<br/>Remember to handle any sensitive tenant data with confidentiality and ensure compliance with privacy regulations.|
| Owner | Nguyen Hoang Phuc|
| Interation | changes  information  |
| Estimate | |


# 21dthd4-2180607692
##Họ tên: Dương Hoàng Long  
##MSSV:  2180607692
##Lớp: 21DTHD4

| Title | 	Manager make rent bill |
| --- | --- |
| Value Statement | As a Manager, I want to create payment receipts for tenant |
| Acceptance Criteria |<ins>Acceptance Criteria:</ins>When the tenant wants the rent bill,<br>Create and fill in the tenant's name address, invoice date, and total amount payable |
| owner | Dương Hoàng Long |
| UI | ![MakeBill](https://gitlab.com/longmsi147/21dthd4-2180607692/-/raw/main/Pic.jpg?ref_type=heads)|

|N| Req ID | Test Objected | Test Steps | Expected Result | Actual Result | Pass/Fail | Related Defects |
|-| ---| --- | --- | --- | --- | --- | --- | 
|1| Req-01 | Total invoice successfully calculated | 1. Enter the full number . <br/> 2. Click the total button. | The calculation is complete | | | |
|2| Req-02 | Total invoice failed calculated | 1. Enter numbers along with letters <br/> 2. Click the total button. | The calculation fails and displayed error message “Please enter without letters” | | | |
|3| Req-03 | Missing input | input not enough. | error message "Missing input" | | | |
